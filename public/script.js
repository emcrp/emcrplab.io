const viewNames = ['example'];
const views = {};

const uniqueKeys = (entries) => {
  let keys = new Set();
  // gather unique keys
  entries.forEach((entry) => {
    Object.keys(entry).forEach(key => keys.add(key));
  });
  return Array.from(keys);
};

Handlebars.registerHelper('eachkey', function(entries, options) {
  return uniqueKeys(entries).map(key => options.fn(key)).join('');
});

Handlebars.registerHelper('eachbykey', function(entries, entry, options) {
  return uniqueKeys(entries).map(key => options.fn(entry[key])).join('');
});


const includeIntoDiv = (divPrefix, htmlName) => {
  // load body from appropriately named HTML
  $('#' + divPrefix + '_body').load(htmlName);

  // switch to invisible
  $('#' + divPrefix + '_body').toggle();

  // header toggles section visibility
  $('#' + divPrefix + '_header').click(function () {
    $('#' + divPrefix + '_body').toggle();
  });
};

window.onload = () => {
	includeIntoDiv('halloffame', 'html/halloffame.html');

  ['ecal','naco','ecal2017','synasc2018','gecco2019'].forEach((value) => {
    includeIntoDiv(value, `html/midi/${value}.html`);
  });

	['immediate','indirect','stack','sbnzoisc','musicdsl'].forEach((value) => {
		includeIntoDiv(`${value}oci`, `html/oci/${value}oci.html`);
		includeIntoDiv(`${value}harvardoci`, `html/oci/${value}harvardoci.html`);
  });

  /*
  viewNames.forEach((viewName) => {
    fetch(`views/${viewName}.hbs`)
      .then(response => response.text())
      .then((view) => {
        views[viewName] = Handlebars.compile(view);
      });
  });

  fetch('model/example.json')
    .then(response => response.json())
    .then((entries) => {
      const result = views.example(entries);
      console.log(result);
    });*/

};